//
//  ItemListController.swift
//  projekti
//
//  Created by iosdev1 on 2.12.2020.
//  Copyright © 2020 iosdev1. All rights reserved.
//

import Foundation
import UIKit

class ItemListController : UIViewController {
    
    
    
    @IBOutlet var tableView: UITableView?
    
    var listOfItems = [ItemDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let requestingItems = ItemRequest()
        requestingItems.getItems { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let items):
                self?.listOfItems = items
            }
        }
        
        
    }

}

extension ItemListController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10 //Change later!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCellIdentifier", for: indexPath) as! ItemCell
        let item = listOfItems[indexPath.row]
        
        cell.itemTitleLable.text = item.id
        cell.itemImageView.image = UIImage(named: item.image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetails", sender: Self.self)
    }
    //override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //if let destination = segue.destination as? ItemDetailPageController {
            //destination.it
       // }
    //}
}
