//
//  itemRequest.swift
//  projekti
//
//  Created by iosdev1 on 9.12.2020.
//  Copyright © 2020 iosdev1. All rights reserved.
//

import Foundation

enum zircleError:Error {
    case noDataAvailable
    case cannotProcessData
}

struct ItemRequest {
    let resourceURL:URL
    let API_KEY = "9hC4HxmqRe7NG3HDPg9c6WEj"
    
    init() {
        let resourceString = "https://api.zalando-wardrobe.de/api/marketplace/groups/all/all_items/search?page_size=20&page_number=1&sort=created_at&sort_direction=desc&b2c=true&c2c=true"
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
    
    func getItems (completion: @escaping(Result<[ItemDetails], zircleError>) -> Void){
        let dataTask = URLSession.shared.dataTask(with: resourceURL) {data, _, _ in
        guard let jsonData = data else {
            completion(.failure(.noDataAvailable))
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let zircleResponse = try decoder.decode(ZircleItems.self, from: jsonData)
            let itemDetails = zircleResponse.item
            completion(.success(itemDetails))
        } catch{
            completion(.failure(.cannotProcessData))
            }
    }
        dataTask.resume()
}
}
