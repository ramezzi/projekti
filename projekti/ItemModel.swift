//
//  Item.swift
//  projekti
//
//  Created by iosdev1 on 7.12.2020.
//  Copyright © 2020 iosdev1. All rights reserved.
//

import Foundation


    
    
    struct ZircleItems:Decodable {
        var item:[ItemDetails]
    }
    
    struct ItemDetails:Decodable {
        var id: String
        var brand: String
        var category: String
        var size: String
        var price: String
        var image: String
    }
    
    
    
    
  

