//
//  ItemCell.swift
//  projekti
//
//  Created by iosdev1 on 7.12.2020.
//  Copyright © 2020 iosdev1. All rights reserved.
//

import Foundation
import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var itemTitleLable: UILabel!
}
